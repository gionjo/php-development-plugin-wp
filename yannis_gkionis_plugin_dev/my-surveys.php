<?php

/*

Plugin Name: My Surveys

Plugin URI: http://yannisgkionis.gr

Description: Create simple surveys and see insightful statistics from them.

Version: 1.0

Author: Yannis Gio

Author URI:

License: GPL2

License URI: https://www.gnu.org/licenses/gpl-2.0.html

Text Domain: My Surveys

*/

 



/*



1. HOOKS

      1.1 - admin menus and pages

      1.2 - plugin activation

      1.3 - shortcodes

      1.4 - load external scripts

      1.5 - register ajax function





2. SHORTCODES

      2.1 - mysurv_register_shortcodes()

      2.2 - mysurv_survey_shortcode()





3. FILTERS

      3.1 - mysurv_admin_menus()



4. EXTERNAL SCRIPTS

      4.1 - mysurv_admin_scripts()

      4.2 - mysurv_public_scripts()



5. ACTIONS

      5.1 - mysurv_create_plugin_tables()

      5.2 - mysurv_activate_plugin()

      5.3 - mysurv_ajax_save_response()

      5.4 - mysurv_save_response()



6. HELPERS

      6.1 - mysurv_get_question_html()

      6.2 - mysurv_question_is_answered()

      6.3 - mysurv_return_json()

      6.4 - mysurv_get_client_ip()

      6.5 - mysurv_get_response_stats()

      6.6 - mysurv_get_item_responses()

      6.7 - mysurv_get_survey_responses()



7. CUSTOM POST TYPES



      7.1 - mysurv_survey



8. ADMIN PAGES

    8.1 - mysurv_welcome_page()

    8.2 - mysurv_stats_page()



9. SETTINGS



10. MISCELLANEOUS

   10.1 - mysurv_debug()


*/









/* !1. HOOKS */



// 1.1

// hint: register custom admin menus and pages

add_action('admin_menu', 'mysurv_admin_menus');



// 1.2

// hint: plugin activation

register_activation_hook( __FILE__, 'mysurv_activate_plugin' );



// 1.3

// hint: register shortcodes

add_action('init', 'mysurv_register_shortcodes');



// 1.4

// hint: load external scripts

add_action('admin_enqueue_scripts', 'mysurv_admin_scripts');

add_action('wp_enqueue_scripts', 'mysurv_public_scripts');


// 1.5

// hint: register ajax functions
add_action( 'wp_ajax_mysurv_ajax_save_response', 'mysurv_ajax_save_response' ); //admin user
add_action( 'wp_ajax_nopriv_mysurv_ajax_save_response', 'mysurv_ajax_save_response' ); //website user


/* !2. SHORTCODES */



// 2.1

// hint: registers custom shortcodes for this plugin

function mysurv_register_shortcodes() {



  // hint: [mysurv_survey id="123"]

  add_shortcode('mysurv_survey', 'mysurv_survey_shortcode');

}



// 2.2

// hint: displays a survey

function mysurv_survey_shortcode( $args, $content='' ) {



    // setup our return variable

    $output = '';



    try {



      // begin building our output html

      $output = '<div class="mysurv mysurv-survey">';



      // get the survey id

      $survey_id = ( isset($args['id']) ) ? (int)$args['id'] : 0;



      // get the survey object

      $survey = get_post($survey_id);



      // IF the survey is not a valid mysurv_survey post, return a message

      if( !$survey_id || $survey->post_type !== 'mysurv_survey' ):



          $output .= '<p>The requested survey does not exist.</p>';



      else:



        // build form html

        $form = '';



        if(strlen($content)):

          $form = '

              <div class="mysurv-survey-content">

              '. wpautop($content) .'

              </div>

              ';

            endif;



      $submit_button = '';

      $responses = mysurv_get_survey_responses($survey_id);

        if( !mysurv_question_is_answered( $survey_id ) ):

          $submit_button = '

          <div class="mysurv-survey-footer">
              <p><em>Submit your response and see the results of all '.$responses.' participants of the survey.</em></p>

              <p class="mysurv-input-container mysurv-submit">
                <input type="submit" name="mysurv_submit" value="Submit Your Response" />
              </p>

          </div>

          ';

        endif;

      //secure survey
      //$nonce = wp_nonce_field( 'mysurv-save-survey-submission_'.$survey_id, '_wpnonce', true, false );
      //'.$nonce.'
      $form .= '

      <form id="survey_'.$survey_id.'" class="mysurv-survey-form">
        
        '. mysurv_get_question_html( $survey_id) . $submit_button . '

        </form>

        ';



        // append form html to $output

        $output .= $form;



      endif;



      // close out output html div

      $output .= '</div>';



    } catch( Exception $e ) {





    }



    // return output

    return $output;

}







/* !3. FILTERS */



// 3.1

// hint: registers custom plugin admin menus

function mysurv_admin_menus() {



  /* main menu */

  $top_menu_item = 'mysurv_welcome_page';

  add_menu_page( '', 'My Surveys', 'manage_options', $top_menu_item, $top_menu_item, 'dashicons-chart-bar');



  /* submenu items */



  // welcome

  add_submenu_page( $top_menu_item, '', 'Welcome', 'manage_options', $top_menu_item, $top_menu_item );

  // surveys

  add_submenu_page( $top_menu_item, '', 'Surveys', 'manage_options', 'edit.php?post_type=mysurv_survey' );



  // stats

  add_submenu_page( $top_menu_item, '', 'Stats', 'manage_options', 'mysurv_stats_page', 'mysurv_stats_page' );

}





/* !4. EXTERNAL SCRIPTS */



// 4.1

// hint: loads external files into wordpress ADMIN

function mysurv_admin_scripts() {



  // register scripts with WordPress's internal library

  wp_register_script('mysurv-js-private', plugins_url('/js/private/mysurv.js', __FILE__), array('jquery'),'',true);



  // add to que of scripts that get loaded into every admin page

  wp_enqueue_script('mysurv-js-private');



}



// 4.2

// hint: loads external files into PUBLIC WEBSITE

function mysurv_public_scripts() {



  // register scripts with WordPress's internal library

  wp_register_script('mysurv-js-public', plugins_url('/js/public/mysurv.js', __FILE__), array('jquery'),'',true);

  wp_register_style('mysurv-css-public', plugins_url('/css/public/mysurv.css',__FILE__));



  // add to que of scripts that get loaded into every public page

  wp_enqueue_script('mysurv-js-public');

  wp_enqueue_style('mysurv-css-public');



}







/* !5. ACTIONS */



// 5.1

// hint: installs custom plugin database tables
function mysurv_create_plugin_tables()  {


    global $wpdb;


    // setup return value

    $return_value = false;



    try {



      // get the appropriate charset for your database

      $charset_collate = $wpdb->get_charset_collate();



      // $wpdb->prefix returns the custom database prefix

      // originally setup in your wp-config.php


      $pre = $wpdb->prefix.'mysurv_survey_responses';
      // sql for our custom table creation

      $sql = "CREATE TABLE ".$pre." (
      id mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT,
      ip_address varchar(32) NOT NULL,
      survey_id mediumint(11) UNSIGNED NOT NULL,
      response_id mediumint(11) UNSIGNED NOT NULL,
      created_at TIMESTAMP,
      updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (id),
      UNIQUE INDEX ix (ip_address,survey_id)
      ) $charset_collate;";



      // make sure we include wordpress functions for dbDelta
      
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
     


      // dbDelta will create a new table if none exists or update an existing one

      dbDelta($sql);



      // return true

      $return_value = true;



    } catch( Exception $e ) {



      // php error

    }



    // return result

    return $return_value;

}

function save_output_buffer_to_file()
{
    file_put_contents(
      ABSPATH. 'wp-content/plugins/activation_output_buffer.html'
    , ob_get_contents()
    );
}
add_action('activated_plugin','save_output_buffer_to_file');

// 5.2

// hint: runs functions for plugin activation

function mysurv_activate_plugin() {



  // create/update custom plugin tables

  mysurv_create_plugin_tables();


}

// 5.3
// hint: ajax form handler for saving request responses
// expects: $_POST['survey_id'] and $_POST['response_id']
function mysurv_ajax_save_response(){
 
  $result = array(
    'status'=>0,
    'message'=>'Could not save response',
    'survey_complete'=>false
  );
 
 
  try{
 
      $survey_id = (isset($_POST['survey_id'])) ? (int)$_POST['survey_id'] : 0;
      $response_id = (isset($_POST['response_id'])) ? (int)$_POST['response_id'] : 0;

      //verify nonce
      // if(!check_ajax_referer('mysurv-survey-submission'.$survey_id, false, false)):

      //     $result['message'] = 'Nonce invalid';

      // else:

         $saved = mysurv_save_response($survey_id, $response_id);       
     
          
          if($saved):
     
            $survey = get_post($survey_id);

            if( isset($survey->post_type)):
              $complete = true;
     
              $html = mysurv_get_question_html($survey_id);
     
              $result = array(
                'status'=>1,
                'message'=>'Response saved',
                'survey_complete'=>$complete,
                'html'=>$html
              );
     
              if($complete):
                $result['message'] = 'Survey completed';
              endif;
     
            else:
                $result['message'] = 'Invalid survey';
            endif;
     
          endif;


      // endif;

  } catch(Exception $e){
    //php error
  }
 mysurv_return_json($result);
 
}
 
 
// 5.4
// hint: saves single question response
function mysurv_save_response( $survey_id, $response_id ) {
 
  global $wpdb;
  
  $return_value = false;
 
  try {
 
    $ip_address = mysurv_get_client_ip();
 
    //get question post object
    $survey = get_post( $survey_id );
 
    if( $survey->post_type == 'mysurv_survey' ):
 
      //get current timestamp
      $now = new DateTime();
      $ts = $now->format('Y-m-d H:i:s');
 
      //query sql
      $sql = "
        INSERT INTO {$wpdb->prefix}mysurv_survey_responses (ip_address,survey_id,response_id,created_at)
        VALUES ( %s, %d, %d, %s )
        ON DUPLICATE KEY UPDATE survey_id = %d
      ";
 
      //prepare query
      $sql = $wpdb->prepare($sql,$ip_address, $survey_id, $response_id, $ts, $survey_id);
 
      //run query
      $entry_id = $wpdb->query($sql);
     // if(!$entry_id) var_dump('here');
      //If response saved successfully...
      if( $entry_id ):
 
        //return true
        $return_value = true;
     
      endif;
 
    endif;
 
  } catch( Exception $e ) {
 
    //php error
    mysurv_debug( 'mysurv_save_response php error', $e->getMessage());
  }
 
  return $return_value;
}




/* !6. HELPERS */





// 6.1

// hint: returns html for survey question

function mysurv_get_question_html( $survey_id, $force_results = false) {



    $html = '';



    // get the survey post object

    $survey = get_post($survey_id );



    // IF $survey is a valid mysurv_survey post type ...

    if( $survey->post_type == 'mysurv_survey' ):



      // get the survey question text

      $question_text = $survey->post_content;



      // set up our default question options

      $question_opts = array(

        'Strongly Agree'=>5,

        'Somewhat Agree'=>4,

        'Neutral'=>3,

        'Somewhat Disagree'=>2,

        'Strongly Disagree'=>1

      );



      // check if the current user has already answered this survey question
      // or is force_results is true, treat as answered
      $answered = ($force_results) ? true: mysurv_question_is_answered($survey_id);

      
      // default complete class is blank

      $complete_class = '';


      // setup our inputs html

        $inputs = '<ul class="mysurv-question-options">';


      if ( !$answered ):

        

        // loop over all the $question_opts

        foreach( $question_opts as $key=>$value ):

            $stats = mysurv_get_response_stats($survey_id, $value);

            //append input html for each option

            $inputs .= '<li><label><input type="radio" name="mysurv_question_ '. $survey_id .'" value="'. $value .'" /> '. $key .'</label></li>';



          endforeach;



          $inputs .= '</ul>';



        else:



          // survey is complete, add a real complete class

          $complete_class = ' mysurv-question-complete';
          foreach($question_opts as $key=>$value):

            $stats = mysurv_get_response_stats($survey_id, $value);

            $inputs.='<li><label>'.$key. ' - ' . $stats['percentage'] .'</li>';

          endforeach;
        



        endif;



        $html .= '

        <dl id="mysurv_'. $survey_id .'_question" class="mysurv-question'. $complete_class .'">

        <dt>'. $question_text .'</dt>

        <dd>'. $inputs .'</dd>

        </dl>';



    endif;



    return $html;

}



// 6.2

// hint: returns true or false depending on

// whether or not the current user has answered the survey

function mysurv_question_is_answered( $survey_id ) {

    global $wpdb;

    // setup default return value
    $return_value = false;

    try{

      //get user ip address
      $ip_address = mysurv_get_client_ip();

      //mysurv_debug('ip address', $ip_address);
      
      //sql to check if this user completed the survey
      $sql = "  
            SELECT response_id FROM {$wpdb->prefix}mysurv_survey_responses
            WHERE survey_id = %d AND ip_address = %s
      ";

      //prepare query
      $sql = $wpdb->prepare($sql, $survey_id, $ip_address);

      //run query, returns entry id if successful
      $entry_id = $wpdb->get_var($sql);

      //IF query works and entry id is not null
      if($entry_id !== NULL):
        $return_value = $entry_id;
      endif;

    } catch(Exception $e) {
      //php error
    }

    // return result

    return $return_value;



}



// 6.3

// hint: returns json string and exits php processes
function mysurv_return_json( $php_array ) {
 
  //encode result as json string
  $json_result = json_encode( $php_array );
  
  //return result
  die( $json_result );
 
  // stop all other processing
  exit;
}


// 6.4

// hint: get the IP address of the current user
function mysurv_get_client_ip() {
  $ipaddress = '';
  if(getenv('HTTP_CLIENT_IP')):
    $ipaddress = getenv('HTTP_CLIENT_IP');
  elseif(getenv('HTTP_X_FORWARDED_FOR')):
    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
  elseif(getenv('HTTP_X_FORWARDED')):
    $ipaddress = getenv('HTTP_X_FORWARDED');
  elseif(getenv('HTTP_FORWARDED_FOR')):
    $ipaddress = getenv('HTTP_FORWARDED_FOR');
  elseif(getenv('HTTP_FORWARDED')):
    $ipaddress = getenv('HTTP_FORWARDED');
  elseif(getenv('REMOTE_ADDR')):
    $ipaddress = getenv('REMOTE_ADDR');
  else:
    $ipaddress = 'UNKNOWN';
  endif;
  return $ipaddress;
}
 



// 6.5

// hint: get the statistics for a survey response
function mysurv_get_response_stats($survey_id, $response_id){

  //setup default return value
  $stats = array(
      'percentage'=>'0%',
      'votes'=>0
  );

  try{

      //get response for this specific item
      $item_responses = mysurv_get_item_responses($survey_id, $response_id);

      //get total responses for this entire survey
      $survey_responses = mysurv_get_survey_responses($survey_id);

      if($survey_responses && $item_responses):
        $stats = array(
            'percentage'=>ceil(($item_responses/$survey_responses)*100) .'%',
            'votes'=>$item_responses
        );
      endif;

  }catch(Exception $e){
    //php error
    mysurv_debug('mysurv_get response stats exception', $e->getMessage());
  }

  return $stats;
}



// 6.6

// hint: get the specific item responses

function mysurv_get_item_responses($survey_id, $response_id){

  global $wpdb;

  $item_responses = 0;

  try{

    //sql to check if user has completed this survey
    $sql = "   
        SELECT count(id) AS total FROM {$wpdb->prefix}mysurv_survey_responses
        WHERE  survey_id = %d AND response_id = %d
    ";

    //prepare query
    $sql = $wpdb->prepare($sql, $survey_id, $response_id);

    //run query and return total
    $item_responses = $wpdb->get_var($sql);

  }catch(Exception $e){
    //php error
    mysurv_debug('mysurv_get_item_responses error', $e->getMessage());
  }

  return $item_responses;
}



// 6.7

// hint: get the entire survey responses

function mysurv_get_survey_responses($survey_id){

  global $wpdb;

  $survey_responses = 0;

  try{

    //sql to check if user has completed this survey
    $sql = "   
        SELECT count(id) AS total FROM {$wpdb->prefix}mysurv_survey_responses
        WHERE  survey_id = %d
    ";

    //prepare query
    $sql = $wpdb->prepare($sql, $survey_id);

    //run query and return total
    $survey_responses = $wpdb->get_var($sql);

  }catch(Exception $e){
    //php error
    mysurv_debug('mysurv_get_survey_responses error', $e->getMessage());
  }

  return $survey_responses;
}



/* !7. CUSTOM POST TYPES */





// 7.1

// mysurv_survey

include_once( plugin_dir_path( __FILE__ ) . 'inc/mysurv_survey.php');









/* !8. ADMIN PAGES */

// 8.1

// hint: this page explains what the plugin is about

// and provides a snapshot of survey participation for the day

function mysurv_welcome_page() {



  $output = '



    <div class="wrap mysurv-welcome-admin-page">



      <h2>Snappy Surveys</h2>



      <p>Get to know your audience. Create simple surveys that capture anonymous data. See insightful statistics from your surveys.</p>



      <p>0 people participated in surveys today.</p>



      </div>

  ';



    echo $output;

}



// 8.2

// hint: this page displays dynamic survey statistics

function mysurv_stats_page()  {



  $output = '



    <div class="wrap mysurv-stats-admin-page">



      <h2>Survey Statistics</h2>



      <p>

        <label>Select a survey</label>

        <select>

          <option> - Select One - </option>

        </select>

      </p>



      </div>

  ';



    echo $output;

}













/* !9. SETTINGS */









/* !10. MISCELLANEOUS */

// 10.1

// hint: write output to the browser and runs kills php processes
function mysurv_debug($msg='', $data = false, $die = true){
  echo '<pre>';
  if(strlen($msg)):
    echo $msg.'<br/>';
  endif;
  if($data !== false):
    var_dump($data);
  endif;
  echo '</pre>';

  if($die) die();
}