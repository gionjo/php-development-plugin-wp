jQuery(document).ready(function($){
	mysurv_debug('public js script loaded');

	//displays message and data in the console debugger
	function mysurv_debug(msg, data){
		try{
			console.log(msg);
			if(!typeof(data) !== "undefined"){
				console.log(data);
			}
		}catch(e){

		}
	}

	//setup our wp ajax URL
	var wpajax_url = document.location.protocol + '//' + document.location.host + '/project3/wp-admin/admin-ajax.php';
	//don't forget to remove project3

	//bind custom function to survey form submit event
	$(document).on('submit', '.mysurv-survey-form', function(e){

		e.preventDefault();

		$form = $(this);
		$survey = $form.closest('.mysurv-survey');

		//get selected radio button
		$selected  = $('input[name^="mysurv_question_"]:checked', $form);

		//split field name into array
		var name_arr = $selected.attr("name").split('_');
		//get the survey id from the last item in the array
		var survey_id = name_arr[2];
		//get the response id from the value of the selected item
		var response_id = $selected.val();

		var data = {			
			survey_id: survey_id,
			response_id: response_id
		};

		//get the closest dl.mysurv-question element
		$dl = $selected.closest('dl.mysurv-question');

		//submit the chosen item via ajax
		$.ajax({
			cache: false,
			method: 'post',
			url: wpajax_url + '?action=mysurv_ajax_save_response',
			dataType: 'json',
			data: data,
			success: function(response){
				//return response in console for debugging
				mysurv_debug(response);

				//if submission was successful
				if(response.status){
					//update the html
					$dl.replaceWith(response.html);

					//hide survey message
					$('.mysurv-survey-footer',$survey).hide();
				}else{
					alert(response.message);
				}				
			},
			error: function(jqXHR, textStatus, errorThrown){
				//output error info for debugging
				mysurv_debug('error', jqXHR);
				mysurv_debug('textStatus', textStatus);
				mysurv_debug('errorThrown', errorThrown);
			}
		});
	});
	
});

//_wpnonce: $('[name="_wpnonce"]', $form).val(),
//_wp_http_referer: $('[name="_wp_http_referer"]', $form).val(),