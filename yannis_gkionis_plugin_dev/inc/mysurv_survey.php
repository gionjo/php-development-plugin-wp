<?php
function yannis_post_types(){
	//myserv Surveys Post Type
    register_post_type('mysurv_survey',array(
    'supports' => array('title', 'editor', 'excerpt'),
    'rewrite' => array('slug' => 'mysurv_surveys'),
    'has_archive' => false,
    'public' => false,
    'show_in_menu '=> true,
    'show_ui' => true,
    'capability_type' => 'post',
    'labels' => array(
    'name' => 'Surveys',
    'add_new_item' => 'Add New Survey',
    'edit_item' => 'Edit Survey',
    'all_items' => 'All Surveys',
    'singular_name' => 'Survey'),
    'menu_icon' => 'dashicons-pressthis'
    ));

   
}
add_action('init','yannis_post_types');